# Package Control Repository

A custom repository for Package Control (Sublime Text).  
This repository contains all my personal plugins and packages which are not included in Package Control's default channel.


## Usage

- In Sublime: `Command Palette` > `Add Repository`

- Enter URL: 

```
https://gitlab.com/patopest/package-control-custom-repository/-/raw/master/packages.json?ref_type=heads
```


## Useful Documentation

- Package Control docs about [Channels and Repositories](https://packagecontrol.io/docs/channels_and_repositories)
- Package Control's [default channel repository](https://github.com/patopesto/package_control_channel)
- [Submitting a package to Package Control](https://packagecontrol.io/docs/submitting_a_package)
- [Test tool](https://packagecontrol.io/packages/ChannelRepositoryTools)
